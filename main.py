import MySQLdb, os
import sys
from collections import defaultdict
from random import randint


#Get The Duplicate SQL
duplicate_sql = open('Duplicates.sql', 'r')

#Stuff
_sqlquery = duplicate_sql.read()

#Settings 
_server   = '192.168.2.2'
_xbmc_ip  = '192.168.2.2'
_xbmc_port= '8080'
_username = 'root'
_password = 'root'
_table    = 'MyVideos93'
_debug    = 0

con = MySQLdb.connect(_server, _username, _password, _table)
cur = con.cursor()
con.text_factory = str

cur.execute(_sqlquery)
for row in cur:
    MediaType = row[0]
    MediaName = row[1]
    Files     = row[2].split(",")
    Paths     = row[3].split(",")
    Scores    = row[5].split(",")

    #Remove The Best Copy From The Arrays
    BestCopy = Scores.index(max(Scores))
    Files.pop(BestCopy)
    Paths.pop(BestCopy)
    Scores.pop(BestCopy)
    for ToBeDeleted in Paths:
        print
        print
        print row
        print "Deleting %i Files For %s (%s)" % ( len(Files), MediaName, MediaType)
        print "-----%s" % ( ToBeDeleted )
