SELECT main.MediaType, main.Name, GROUP_CONCAT(main.File) as File, GROUP_CONCAT(main.FilePath) as FilePath,  GROUP_CONCAT(main.Object) as Object, GROUP_CONCAT(main.Score) as Score FROM 
    (
        SELECT 
            src.c00 as Name, 
            src.c01 as Descr, 
            src.idFile AS File,
            src.idMovie AS Object,
            src.c22 as FilePath, 
            'Movies' as MediaType,
            ROUND((dtl.iVideoWidth * dtl.iVideoHeight) / 10000, 0) as Score   
        FROM movie src 
        LEFT JOIN streamdetails dtl ON src.idFILE = dtl.idFile 
        WHERE dtl.iStreamType = 0 
    UNION ALL 
        SELECT 
            src.c00 as Name, 
            src.c01 as Descr, 
            src.idFile AS File,
            src.idEpisode AS Object,
            src.c18 as FilePath, 
            'TV' as MediaType,
            ROUND((dtl.iVideoWidth * dtl.iVideoHeight) / 10000, 0) as Score   
        FROM episode src 
        LEFT JOIN streamdetails dtl ON src.idFILE = dtl.idFile 
        WHERE dtl.iStreamType = 0 
    ) 
main group by main.MediaType, main.Name, main.Descr HAVING COUNT(*) > 1